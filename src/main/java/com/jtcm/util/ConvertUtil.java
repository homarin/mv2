package com.jtcm.util;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class ConvertUtil {
	
	public static String objectToJson(Object objArg) {
		Gson gson = new GsonBuilder().create();
		String jsonStr = gson.toJson(objArg);
		return jsonStr;
	}
	
	public static String[] convertArgsIntoArray(Object arg) throws Exception{
		String []arrArg = null;
		
		if ( arg instanceof String[]) {
			arrArg = (String[])arg;
			
		} else if( arg instanceof String) {
			arrArg = new String[1];
			arrArg[0] = (String)arg;
			
		} else if( arg instanceof List) {
			arrArg = new String[((List) arg).size()];
			
			for(int i=0; i < ((List) arg).size(); i++) {
				try {
					arrArg[i] = (String)(((List) arg).get(i) );
				}catch(ClassCastException cce) {
					throw new Exception();
				}
			}
		} else{
			//Logger.getLogger(ConvertUtil.class.getName()).log(Level.INFO, "Map type");
			//Logger.getLogger(ConvertUtil.class.getName()).log(Level.INFO, "arg : " + arg.toString());
			arrArg = new String[1];
			arrArg[0] = objectToJson(arg);
			//Logger.getLogger(ConvertUtil.class.getName()).log(Level.INFO, "arrArg[0] : " + arrArg[0]);
		}		
		
		return arrArg;
	}
	
	public static Object convertArgsIntoJson(String strArgs) {
		Object returnObj = null;
		
		try {
			JsonParser parser = new JsonParser();
			JsonElement jsonEle = parser.parse(strArgs.trim());
			
			if (jsonEle instanceof JsonObject) {
				returnObj =  jsonEle.getAsJsonObject();
			} else if (jsonEle instanceof JsonArray) {
				returnObj = jsonEle.getAsJsonArray();
			}			
			
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		}
		
		return returnObj;
	}
}
