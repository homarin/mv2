package com.jtcm.util;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {

	public static String getNowDateTimeNanos() {
		Clock clock = new NanoClock();   
	    Instant instant = Instant.now(clock);   
	    DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss.SSSSSSSSS").withZone(ZoneId.systemDefault());
		return DATE_TIME_FORMATTER.format(instant);
	}

}
