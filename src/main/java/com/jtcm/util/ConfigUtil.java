package com.jtcm.util;

import java.util.Properties;

public class ConfigUtil {
	private static Properties commonProps = null;
	
	public void setCommonProps(Properties commonProps) {
		ConfigUtil.commonProps = commonProps;
	}	
	
	public static String getCommonProps(String propName) {
		return commonProps.getProperty(propName);
	}
}
