package com.jtcm.core;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jtcm.util.ConvertUtil;
import com.jtcm.util.DateTimeUtil;
import com.lgcns.blockchain.framework.core.client.BlockchainClient;
import com.lgcns.blockchain.framework.core.client.BlockchainClientFactory;
import com.lgcns.blockchain.framework.core.dto.BlockchainDto;
import com.lgcns.blockchain.framework.core.exception.BlockchainException;
import com.lgcns.blockchain.framework.fabric.config.FabricConfig;

public class BlockChainAdapter {
	
	private static Logger LOG = LoggerFactory.getLogger(BlockChainAdapter.class);
	
	//private static final String DEFAULT_PROFILE = "default";
	private static final String DEFAULT_PROFILE = "user1";
	
	//private static BlockchainClient client;
	
	public JsonObject write(String ccName, String funcNm, Object args) {
		long writeTimeout = FabricConfig.getWriteTimeout(DEFAULT_PROFILE);
		return write(ccName, funcNm, args, writeTimeout, DEFAULT_PROFILE);
	}
	
	public JsonObject read(String ccName, String funcNm, Object args) {
		long writeTimeout = FabricConfig.getWriteTimeout(DEFAULT_PROFILE);
		return read(ccName, funcNm, args, writeTimeout, DEFAULT_PROFILE);
	}	
	
	private JsonObject write(String ccName, String funcNm, Object args, long timeout, String profileName) {
		System.out.println("BlockChainAdapter :: write - start");
		
		BlockchainClient client = null;
		String[] arrArg = null;

		String target = ccName + "." + funcNm;
		System.out.println("BlockChainAdapter :: write - target: " + target);
		
		JsonObject returnJson = new JsonObject();
		
		returnJson.addProperty("success", true);
		returnJson.addProperty("errorCode", "");
		returnJson.addProperty("errorMsg", "");
		
		try {
			BlockchainDto input = null;

			Map<String, byte[]> transientMap = new HashMap<>();
			transientMap.put("_dateTimeNano", DateTimeUtil.getNowDateTimeNanos().getBytes(UTF_8));
			
			if( args == null ) {
				input = new BlockchainDto.Builder().targetFunction(target).option("transientmap", transientMap).timeout(timeout).build();
			}else {
				arrArg = ConvertUtil.convertArgsIntoArray(args);
				input = new BlockchainDto.Builder().targetFunction(target).arguments(arrArg).option("transientmap", transientMap).timeout(timeout).build();
			}
			
			client = BlockchainClientFactory.getClient(profileName);
            client.write(input);
		} catch ( ClassCastException cce) {
            LOG.error("client error", cce);
            LOG.error("error error message : {}", cce.getMessage());
            
            returnJson.addProperty("success", false);
            returnJson.addProperty("errorCode", cce.getClass().getName());
            returnJson.addProperty("errorMsg", cce.getMessage());			
			
        } catch (BlockchainException be) {
            /************************************************************************************************************************
             * read/write로 발생하는 모든 runtime error를 래핑하여 BlockchainException로 발생시킨다  => BlockchainException만 처리
             * 업무 로직 상 에러의 상세 내용이 필요할 경우, getCode 및 getMessage를 통해 추출하여 사용한다
             *************************************************************************************************************************/               	
            
        	
        	LOG.error("client error", be);
            LOG.error("error code : {}, error message : {}",be.getCode(), be.getMessage());
            
            returnJson.addProperty("success", false);
            
            if ( be.getMessage().startsWith("[") ) {
            	String orgErrorMsg = be.getMessage();
            	int indexClose = orgErrorMsg.indexOf("]");
            	String newErrorCode = orgErrorMsg.substring(1, indexClose);
            	String newErrorMsg = orgErrorMsg.substring(indexClose+1);
            	if( !newErrorCode.trim().equals("")) {
                    returnJson.addProperty("errorCode", newErrorCode);
                    returnJson.addProperty("errorMsg", newErrorMsg);            		
            	}else {
                    returnJson.addProperty("errorCode", be.getCode());
                    returnJson.addProperty("errorMsg", be.getMessage());            		
            	}
            } else {
                returnJson.addProperty("errorCode", be.getCode());
                returnJson.addProperty("errorMsg", be.getMessage());            	
            }
    		
        } catch (Exception e) {
            LOG.error("client error", e);
            LOG.error("error message : {}", e.getMessage());
            
            returnJson.addProperty("success", false);
            returnJson.addProperty("errorMsg", e.getMessage());    		
    		
        } finally {
            //client release
        	LOG.info("releaseClient");
            BlockchainClientFactory.releaseClient(client);
        }
		return returnJson;
	}	
	
	private JsonObject read(String chainCode, String funName, Object args, long timeout, String profileName) {
		System.out.println("BlockChainAdapter :: read - start");
		
		BlockchainClient client = null;
		String[] arrArg = null;
		String resultJsonStr = "";
		
		String target = chainCode + "." + funName;
		System.out.println("BlockChainAdapter :: read - target: " + target);

		JsonObject returnJson = new JsonObject();

		returnJson.addProperty("success", true);
		returnJson.addProperty("errorCode", "");
		returnJson.addProperty("errorMsg", "");
		
		try {
			BlockchainDto query = null;
			if( args == null ) {
				query = new BlockchainDto.Builder().targetFunction(target).timeout(timeout).build();
			}else {
				arrArg = ConvertUtil.convertArgsIntoArray(args);
				query = new BlockchainDto.Builder().targetFunction(target).arguments(arrArg).timeout(timeout).build();
			}
			
			client = BlockchainClientFactory.getClient(profileName);
			resultJsonStr = new String((byte[])client.read(query));
		} catch ( ClassCastException cce) {
            LOG.error("client error", cce);
            LOG.error("error error message : {}", cce.getMessage());
            
            returnJson.addProperty("success", false);
            returnJson.addProperty("errorCode", cce.getClass().getName());
            returnJson.addProperty("errorMsg", cce.getMessage());			
			
        } catch (BlockchainException be) {
            /************************************************************************************************************************
             * read/write로 발생하는 모든 runtime error를 래핑하여 BlockchainException로 발생시킨다  => BlockchainException만 처리
             * 업무 로직 상 에러의 상세 내용이 필요할 경우, getCode 및 getMessage를 통해 추출하여 사용한다
             *************************************************************************************************************************/               	
            LOG.error("client error", be);
            LOG.error("error code : {}, error message : {}",be.getCode(), be.getMessage());
            
            returnJson.addProperty("success", false);
            
            if( be.getMessage().startsWith("[") ) {
            	String orgErrorMsg = be.getMessage();
            	int indexClose = orgErrorMsg.indexOf("]");
            	String newErrorCode = orgErrorMsg.substring(1, indexClose);
            	String newErrorMsg = orgErrorMsg.substring(indexClose+1);
            	if( !newErrorCode.trim().equals("")) {
                    returnJson.addProperty("errorCode", newErrorCode);
                    returnJson.addProperty("errorMsg", newErrorMsg);            		
            	}else {
                    returnJson.addProperty("errorCode", be.getCode());
                    returnJson.addProperty("errorMsg", be.getMessage());            		
            	}
            }else {
                returnJson.addProperty("errorCode", be.getCode());
                returnJson.addProperty("errorMsg", be.getMessage());            	
            }
    		
        } catch( Throwable e){
            e.printStackTrace();

        } finally {
        	LOG.info("releaseClient");
            BlockchainClientFactory.releaseClient(client);
        }
		
		if( resultJsonStr != null ) {
			JsonElement paramObj = (JsonElement) ConvertUtil.convertArgsIntoJson(resultJsonStr);
			returnJson.add("data", paramObj);
		}
		
		return returnJson;
	}	
}
