package com.jtcm.sample;

import java.util.Scanner;

import com.google.gson.JsonObject;
import com.jtcm.core.BlockChainAdapter;
import com.jtcm.util.ConvertUtil;

/**
 * ------------------------------------------------------------
 *  - CLASS NAME  : SampleChainCodeClient
 *  - DESCRIPTION : '블록체인의 조회/신규/수정/삭제'를 위한 샘플
 *  - HISTORY
 * ------------------------------------------------------------
 * ------------------------------------------------------------
 * 	DATA			AUTHOR			DESCRIPTION
 * ------------------------------------------------------------
 * 2019.05.10		정상락		최초작성
 * ------------------------------------------------------------
 */




public class SampleChainCodeClient {
	private static BlockChainAdapter adapter;
    //private static Logger LOG = LoggerFactory.getLogger(SampleChainCodeClient.class);
    
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// local
		System.setProperty("org.hyperledger.fabric.sdk.configuration", "C:\\dev\\workspace\\jtcm.telaio.sdk\\local-config.properties");
		System.setProperty("com.lgcns.blockchain.framework.configuration", "C:\\dev\\workspace\\jtcm.telaio.sdk\\local-framework-config.properties");
    	
		// devmode
		//System.setProperty("org.hyperledger.fabric.sdk.configuration", "C:\\dev\\workspace\\jtcm.telaio.sdk\\localdevmode-config.properties");
		//System.setProperty("com.lgcns.blockchain.framework.configuration", "C:\\dev\\workspace\\jtcm.telaio.sdk\\localdevmode-framework-config.properties");
    	adapter = new BlockChainAdapter();
    	
    	printHelp();
    	
    	String inputs = null;
    	String result = null;

    	Scanner scan = null;
    	
    	while (true) {
    		scan = new Scanner(System.in);
    		inputs = scan.nextLine();
    		
    		if (inputs.isEmpty()) {
    			result = "no argument! Please, try again.";
    		} else {
    			args = getParams(inputs);
    			String execType = args[0];
    			
    			if ("read".equals(execType)) {
    				JsonObject paramObj = (JsonObject) ConvertUtil.convertArgsIntoJson(args[3]);
    				result = clientReadStart(args[1], args[2], paramObj);
    				printResult(result);
    			} else if ("write".equals(execType)){
    				JsonObject paramObj = (JsonObject) ConvertUtil.convertArgsIntoJson(args[3]);
    				result = clientWriteStart(args[1], args[2], paramObj);
    				printResult(result);
    			} else if ("help".equals(execType)){
    				printHelp();
    			} else {
    				break;
    			}
    		}
    	}
    	System.out.println("Bye~~");
    } 
    
	private static String[] getParams(String cmdLine) {
		cmdLine = cmdLine.replaceAll(" " , "");
		cmdLine = cmdLine.replaceAll("\\p{Z}", "");
		String[] cmds = cmdLine.split("\\|\\|");
		return cmds;
	}

	/****************************************************
	 * - FUNC NAME : clientReadStart
	 * - DESCRIPTION : 블록체인 조회 함수
	 * - AUTHOR : 정상락
	 * - DATE : 2019-05-10 
	 ****************************************************/            
	
    private static String clientReadStart(String ccName, String funcName, Object params) {
    	
    	String result = "";
        
        JsonObject getAssetResult = adapter.read(ccName, funcName, params);
        result = getAssetResult.toString();
        
        return result;
    }
    
    private static String clientWriteStart(String ccName, String funcName, Object params) {
    	/************************************************************************************************************************
    	 * write 예시 (블록체인 - 신규/수정/삭제)
    	 *************************************************************************************************************************/
    	String result = "";
        
        JsonObject registerAssetResult = adapter.write(ccName, funcName, params);
        result = registerAssetResult.toString();
        
        return result;
    }    
    
	private static void printHelp() {
		System.out.println("------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("Input your Command");
		System.out.println("=> MethodType||chaincodeName||funcName||params");
		System.out.println("1. read");
		System.out.println("   ex) read||sample_cc||getAsset||{\"assetNumber\":\"00001\"}");
		System.out.println("2. write");
		System.out.println("   ex) write||sample_cc||registerAsset||{\"assetNumber\":\"00002\",\"assetName\":\"testAsset\",\"assetValue\":\"100000\",\"receivingDate\":\"20190422\"}");
		System.out.println("------------------------------------------------------------------------------------------------------------------------------");
		System.out.print("=> ");		
	}   
	
	private static void printResult(String result) {
		System.out.println("result =>");
		System.out.println(result);		
		System.out.println();
		System.out.println("------------------------------------------------------------------------------------------------------------------------------");
		System.out.print("=> ");  		
	}	
}
